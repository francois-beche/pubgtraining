import { connect } from 'react-redux'
import React from 'react';
import  Name  from '../components/name'
import {userName} from '../actions'
import BaseComponent from '../utils/BaseComponent'


class NameContainer extends BaseComponent{

    constructor(props){
        super(props);
        this.id = "1";
        this.handleClick = this.handleClick.bind(this);
        this.state ={
            resp : null
            }
    }
    componentDidMount (){
        setTimeout(() => { console.log("timeout");  }, 3000);
        this.handleClick();
    }
  
    handleClick = async event => {
        if (event  != null)
            event.preventDefault();
      
        // Promise is resolved and value is inside of the response const.
        const response = await this.callGet(`users/${this.id}`);

        console.log(response);
        console.log(response.data);
        this.setState({resp : response.data});
      };
    render() {
      //  const username = this.props.username; => equivalent à la ligne du dessous
        const {username, dispatch } = this.props
      return (
      <div>
          <Name username={username} /> 
          <button onClick={this.test}> test base component </button>
          <button onClick={e => {
              setTimeout(() => { console.log("timeout"); dispatch(userName("Francois_with_latence")); }, 3000);
              }}> insert name in store after 3000ms</button>

              <button onClick={this.handleClick}> Fetching data </button>
              <div>{ this.state.resp != null ? this.state.resp['name']: "null"}</div>
    </div>
    );
    }
}

const getName = state => ({
    username : state.userName.username
})

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: dispatch,
    }
}
export default connect(getName, mapDispatchToProps)(NameContainer)