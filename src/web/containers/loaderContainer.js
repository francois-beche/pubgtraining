import { connect } from 'react-redux'
import React from 'react';
import Loader from '../components/Loader'
import { loader } from '../actions'
import BaseComponent from '../utils/BaseComponent'

class LoaderContainer extends BaseComponent{

    render(){
        const {activeLoader, toggleLoader} = this.props
        return (<div><Loader active={activeLoader}  toggle={toggleLoader}/> </div>)
    }
}
 //state . nomDeLaFonctionAction .
const getActiveLoader = state => ({
    activeLoader : state.loader.loader
})


const mapDispatchToProps = (dispatch) => {

    return {
        dispatch: dispatch,
        toggleLoader : val => dispatch(loader(val))
    }
}

export default connect(getActiveLoader, mapDispatchToProps)(LoaderContainer)