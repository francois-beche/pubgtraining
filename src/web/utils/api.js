import axios from 'axios';

const AuthStr = 'Bearer '.concat("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJjMWQ3ZDZkMC01MzBjLTAxMzYtMWE3Yy0zNzMyZGJjOWFlZTYiLCJpc3MiOiJnYW1lbG9ja2VyIiwiaWF0IjoxNTI5MDk2Mjk5LCJwdWIiOiJibHVlaG9sZSIsInRpdGxlIjoicHViZyIsImFwcCI6ImRpY2tjb21wYXJlIn0.i3jOue8Z51Cs_tVSsI2yM-1y-0Gg0de9k7giACWhK8Y");

export default axios.create({
  baseURL: `https://api.playbattlegrounds.com/shards/`,
  headers: {
    Authorization: AuthStr,
    Accept: "application/vnd.api+json"
  }
});