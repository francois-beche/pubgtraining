
import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './Home'
import Roster from './Roster'
import Player from './Player'
import PlayerFinder from './PlayerFinder'

const Main = () => (
    <main>
      <Switch>
        <Route exact path='/' component={Home}/>
        <Route path="/home" component={Home}/>
        <Route path='/roster' component={Roster}/>
        <Route path='/player' component={Player}/>
        <Route path='/playerFinder' component={PlayerFinder}/>
      </Switch>
    </main>
  )
  
  export default Main