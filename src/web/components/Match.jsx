import React from 'react'
import { connect } from 'react-redux'
import BaseComponent from '../utils/BaseComponent'
//import axios from 'axios';

import PlayerInfo from './PlayerInfo'
import { match as createMatch, } from '../actions'
import { bindActionCreators, } from 'redux';
class Match extends BaseComponent {

    constructor(props) {
        super(props);
        this.state = {
            displayStat: false,
            avgDmgDealt: 0,
            playerStats: null
        }
        this.handleClick = this.handleClick.bind(this);
        this.display = this.display.bind(this);
    }
    componentDidMount() {

    }
    handleClick(e) {
        console.log("clicked");
        this.props.match({
            matchId: this.props.matchId,
            playerName: "Bronovitch"
        });

    }
    /*
        getStat = async (name) => {
    
            this.setState({ avgDmgDealt: 0 });
            const AuthStr = 'Bearer '.concat("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJjMWQ3ZDZkMC01MzBjLTAxMzYtMWE3Yy0zNzMyZGJjOWFlZTYiLCJpc3MiOiJnYW1lbG9ja2VyIiwiaWF0IjoxNTI5MDk2Mjk5LCJwdWIiOiJibHVlaG9sZSIsInRpdGxlIjoicHViZyIsImFwcCI6ImRpY2tjb21wYXJlIn0.i3jOue8Z51Cs_tVSsI2yM-1y-0Gg0de9k7giACWhK8Y");
            const URL = "https://api.playbattlegrounds.com/shards/pc-eu/matches/" + this.props.matchId;
            //dac0cddd-fcbb-424d-9b50-731d817efe6c
            const response = await axios.get(URL, { headers: { Authorization: AuthStr, Accept: "application/vnd.api+json" } });
    
            console.log(response.data.included[0]);
            const data = response.data.included;
            var total = 0;
            var nb = 0;
    
            data.forEach((element) => {
                if (element.type === "participant") {
                    nb = nb + 1;
                    total = total + element.attributes.stats.damageDealt;
                    if (name !== null && element.attributes.stats.name === name){
                        console.log("LOL=>" +element.attributes.stats.name)
                        this.setState({ playerStats: element.attributes.stats });
                    }
                        
                }
                console.log(element);
            });
            total = total / nb;
            console.log("total = " + total + "  playername =" + this.props.playerName)
            this.setState({ avgDmgDealt: total })
            this.setState({ displayMatchStat: true });
        }*/

    display() {

        return (<PlayerInfo playerName={this.props.playerName} avgDmgDealt={this.props.avgDmgDealt !== undefined ? this.props.avgDmgDealt : 0} playerStat={this.props.playerStats !== undefined ? this.props.playerStats : {}} />)

    }
    render() {

        return (
            <div>
                <button onClick={this.handleClick}> Match ({this.props.matchId})</button>
                {this.props.avgDmgDealt !== undefined ? this.props.avgDmgDealt : 0}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    console.log(state);
    return {
        status: true,
        avgDmgDealt: state.match.avgDmgDealt,
        playerStats: state.match.playerStats,
        displayMatchStat: true
    };
};

const mapDispatchToPropsTransient = (dispatch) => {
    return {
        match: bindActionCreators(createMatch, dispatch),
    };
};

export default connect(mapStateToProps, mapDispatchToPropsTransient)(Match)
