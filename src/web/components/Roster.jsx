import React from 'react'
import { connect } from 'react-redux'
import {userName, loader} from '../actions'
import NameContainer from '../containers/NameContainer'

/*
const getVisibleTodos = (username) => {
  return username;
}
*/

const getUserName = state => ({
  username: state.userName.username,
  loaderActive: state.loader.loader
})

const Roster = ({dispatch, username, loaderActive}) => (
  <div>
    <h1>Roster</h1>
    <button onClick={ e => {console.log("clicked !"); dispatch(userName("Francois")); console.log(username)}}> Insert name in Store </button>
    <button onClick={ e => {console.log("active loader clicked !"); dispatch(loader(!loaderActive)); }}>active loader</button>
    <NameContainer />
    <br/>

  </div>
)


export default connect(getUserName)(Roster)