import React from 'react'
import { connect } from 'react-redux'
import BaseComponent from '../utils/BaseComponent'
import MatchList from './MatchList';
import '../App.css'

import { matchList as createMatchList, } from '../actions'
import { bindActionCreators, } from 'redux';

class PlayerFinder extends BaseComponent {

    constructor(props) {
        super(props);
        this.state = {
            playerName: '',
            matchArray: null,
            displayMatchList: false
        }

        this.onChange = this.onChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
      //  this.getPlayer = this.getPlayer.bind(this);
    }
    componentDidMount() {

    }

    onChange(e) {
        console.log(e.target.value);
        this.setState({ playerName: e.target.value })
    }


/*
    getPlayer = async () => {
        const response = await this.callGet(`pc-eu/players?filter[playerNames]=${this.state.playerName}`);
        console.log(response.data.data[0].relationships.matches.data);
        this.setState({ matchArray: response.data.data[0].relationships.matches.data, displayMatchList: true });
    };*/



    render() {
        return (
            <div className="matchList">
                nom du joueur : <input type="text" value={this.state.playerName} onChange={this.onChange} />
                <button onClick={this.handleClick}> Rechercher </button>
                <MatchList matchList={this.props.list === undefined ? [] : this.props.list} playerName={this.state.playerName} />
            </div>
        );
    }
    handleClick(e) {
        console.log("clicked");
        //   this.getPlayer();

        this.props.matchList({
            playerName: "Bronovitch"
        });
        e.preventDefault();

    }
}

const mapStateToProps = (state) => {
    return {
        status: true,
        list: state.matchList.matchList
    };
};

const mapDispatchToPropsTransient = (dispatch) => {
    return {
        matchList: bindActionCreators(createMatchList, dispatch),
    };
};
export default connect(mapStateToProps, mapDispatchToPropsTransient)(PlayerFinder)