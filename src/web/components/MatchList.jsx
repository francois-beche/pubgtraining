import React from 'react'
import { connect } from 'react-redux'
import BaseComponent from '../utils/BaseComponent'
import Match from './Match'


class MatchList extends BaseComponent {

    constructor(props) {
        super(props);
        this.state = {

        }
        this.renderArticles = this.renderArticles.bind(this);
    }
    componentDidMount() {

    }

    handleClick(e) {
        if (e != null){
            
        }
    }
    renderArticles(articles) {
        if (articles !== null && articles.length > 0) {      
            return articles.map((article, index) => (
                <Match playerName={this.props.playerName} matchId={article.id}  key={index}/>
            ));
        }
        else return [];
    }
    render() {
        return (
            <div>
             {this.renderArticles(this.props.matchList)}
            </div>
        );
    }
}

export default connect()(MatchList)