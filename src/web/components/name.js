import React from 'react';
import PropTypes from 'prop-types'

//In this container we just fetch data and provide them to
//another component in charge to only display data


const Name = ({username}) => (
    <div>
        <p>name = {username}</p>
    </div>
)


Name.prototypes = {
    username : PropTypes.string.isRequired
}
export default Name;
