import React, { Component } from 'react';
import './App.css';
import LoaderContainer from './containers/loaderContainer';
import Header from './components/Header';
import Main from './components/Main';

class App extends Component {
  render() {
    return (
      <div>
        <LoaderContainer />
        <Header/>
        <Main/>
      </div>

    );
  }
}

export default App;
