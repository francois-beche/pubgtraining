import {Component} from 'react';
import API from '../utils/api';

class BaseComponent extends Component{

    constructor(props){
        super(props);
        this.callGet = this.callGet.bind(this);
    }

    callGet(route, headers){
        return API.get(route);
    }
}

export default BaseComponent;