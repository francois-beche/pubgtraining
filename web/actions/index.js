import API from '../utils/api';

export const userName = name => ({
  type: 'NAME',
  name
})

export const loader = activeLoader => ({
  type: 'LOADER',
  activeLoader
})
/*
export const matchList = matchList =>({
  type: 'MATCH_LIST',
  matchList
})*/

export function matchList({playerName}) {

  return async function (dispatch, getState) {
    try {
      let response = await API.get(`pc-eu/players?filter[playerNames]=${playerName}`)
      console.log(response.data.data[0].relationships.matches.data);
      dispatch({
        type: 'MATCH_LIST',
        matchList: response.data.data[0].relationships.matches.data,
      });

    } catch (err) {
      console.log("error request");
      dispatch({
        type: 'MATCH_LIST',
        matchList: [],
      });
    }
  }
}

export function match({matchId, playerName}) {
  return async function (dispatch, getState) {
    try {
      let response = await API.get(`pc-eu/matches/${matchId}`)
      console.log(response.data.included[0]);
      const data = response.data.included;
      let total = 0;
      let nb = 0;
     let me;
      data.forEach((element) => {
          if (element.type === "participant") {
              nb = nb + 1;
              total = total + element.attributes.stats.damageDealt;
              if (playerName !== null && element.attributes.stats.name === playerName){
                  console.log("LOL=>" +element.attributes.stats.name)
                  me = element.attributes.stats;
                  
              }
          }
         // console.log(element);
      });
      total = total / nb;
      dispatch({
        type :'MATCH',
        avgDmgDealt: total,
        displayMatchStat: true,
        playerStats : me
      })
    } catch (err) {
      console.log("error req match")
      dispatch({
        type :'MATCH',
        avgDmgDealt: 0,
        displayMatchStat: false
      })
    }
  }
}