import React from 'react'
import { connect } from 'react-redux'
import BaseComponent from '../utils/BaseComponent'
import axios from 'axios';

class Player extends BaseComponent{

    constructor(props){
        super(props);
        this.id = "1";
        this.handleClick = this.handleClick.bind(this);
        this.renderArticles = this.renderArticles.bind(this);
        this.getStats = this.getStats.bind(this);
        this.renderStats = this.renderStats.bind(this);
        this.state ={
            displayMatchStat: false,
            resp : null,
            stat : null,
            avgDmgDealt : 0,
            idMatch : null
            }
    }
    componentDidMount (){
        this.handleClick();
    }
  
    handleClick = async event => {
        if (event  != null)
            event.preventDefault();
      
        
        const AuthStr = 'Bearer '.concat("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJjMWQ3ZDZkMC01MzBjLTAxMzYtMWE3Yy0zNzMyZGJjOWFlZTYiLCJpc3MiOiJnYW1lbG9ja2VyIiwiaWF0IjoxNTI5MDk2Mjk5LCJwdWIiOiJibHVlaG9sZSIsInRpdGxlIjoicHViZyIsImFwcCI6ImRpY2tjb21wYXJlIn0.i3jOue8Z51Cs_tVSsI2yM-1y-0Gg0de9k7giACWhK8Y");
        const URL = "https://api.playbattlegrounds.com/shards/pc-na/players?filter[playerNames]=shroud"
       const response = await axios.get(URL, { headers: { Authorization: AuthStr, Accept: "application/vnd.api+json" } });
        console.log(response.data.data[0].relationships.matches.data);
        this.setState({resp : response.data.data[0].relationships.matches.data});
      };

      getStats = async event => {

            if (event  != null)
                event.preventDefault();
            this.setState({avgDmgDealt : 0});
            const AuthStr = 'Bearer '.concat("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJjMWQ3ZDZkMC01MzBjLTAxMzYtMWE3Yy0zNzMyZGJjOWFlZTYiLCJpc3MiOiJnYW1lbG9ja2VyIiwiaWF0IjoxNTI5MDk2Mjk5LCJwdWIiOiJibHVlaG9sZSIsInRpdGxlIjoicHViZyIsImFwcCI6ImRpY2tjb21wYXJlIn0.i3jOue8Z51Cs_tVSsI2yM-1y-0Gg0de9k7giACWhK8Y");
            const URL = "https://api.playbattlegrounds.com/shards/pc-na/matches/"+ event.idMatch;
          //dac0cddd-fcbb-424d-9b50-731d817efe6c
            const response = await axios.get(URL, { headers: { Authorization: AuthStr, Accept: "application/vnd.api+json" } });

            console.log(response.data.included[0]);
            const data = response.data.included;
            var total = 0;
            var nb = 0;
            data.forEach(function(element) {
                if (element.type === "participant"){
                    nb = nb + 1;
                    total = total + element.attributes.stats.damageDealt;
                }
                console.log(element);
              });
total = total / nb;
              console.log("total = " + total )
              this.setState({avgDmgDealt : total})
         this.setState({displayMatchStat : true});
      };

      renderArticles(articles) {
        if (articles !== null && articles.length > 0) {      
            return articles.map((article, index) => (
               <div  key={index}> <span  articleid ={article.id}  onClick={(e) => {
                   e.idMatch = article.id;
                   this.getStats(e);
               }}> {article.id}</span><br /></div>
            ));
        }
        else return [];
    }

    renderStats(articles){

    }
    render() {
      //  const username = this.props.username; => equivalent à la ligne du dessous
     const articles  = this.renderArticles(this.state.resp);
    // const stats = this.renderStats(this.state.stats);
      return (
      <div>
         <h1> Player </h1><br />
        {this.state.displayMatchStat === false ? articles : ''}
      <p> avg damage deatl => {this.state.displayMatchStat !== false ? this.state.avgDmgDealt : ''}</p>
    </div>
    );
    }
}

export default connect()(Player)