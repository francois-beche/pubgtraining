import React from 'react';
import '../App.css'


const Loader = ({active, toggle}) => (
    <div className={active === true? "loader" : "closeLoader"}>
    <button onClick={e => {toggle(false)}}>close loader</button>
    </div>
)

export default Loader