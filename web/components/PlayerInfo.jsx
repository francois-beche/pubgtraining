import React from 'react'
import { connect } from 'react-redux'
import BaseComponent from '../utils/BaseComponent'


class PlayerInfos extends BaseComponent {

    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentDidMount() {

    }

    render() {

        return (
            <div>
                player  {this.props.playerName} dealt = {this.props.playerStat.damageDealt !== null ? this.props.playerStat.damageDealt : ''} top : {this.props.playerStat.winPlace} kills :{this.props.playerStat.kills} <br />
                game avg damage dealt => {this.props.avgDmgDealt}
            </div>
        );
    }
}

export default connect()(PlayerInfos)