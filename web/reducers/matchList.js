const matchList = (state = [], action) => {

  return Object.assign({}, state, {
    matchList: action.matchList
  })

}

export default matchList