import { combineReducers } from 'redux'

 import userName from './userName'
 import loader from './loader'
 import matchList from './matchList'
import match from './match'
export default combineReducers({
  userName,
  loader,
  matchList, match
})