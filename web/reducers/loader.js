const loader = (state = [], action) => {
    return Object.assign({}, state, {
        loader : action.activeLoader
    })
}

export default loader